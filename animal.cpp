///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Kayla Valera <kvalera@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   @todo 20_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "animal.hpp"

using namespace std;

namespace animalfarm {

//Constructor and Destructor defs

	Animal::Animal(){
		cout << ".";

}

	Animal::~Animal(){
		cout << "x";

}
	
void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};

string Animal::bool2string (bool input){
	switch (input){
		case 0: return "FALSE";
		case 1: return "TRUE";
}
 }


string Animal::colorName (enum Color color) {
   switch (color) {
      case BLACK: 
	      return string("Black"); break;
      case WHITE: 
	      return string("White"); break;
      case RED:   
	      return string("Red"); break;
      case BLUE:  
	      return string("Blue"); break;
      case GREEN: 
	      return string("Green"); break;
      case PINK:  
	      return string("Pink"); break;
      case SILVER:
	      return string("Silver"); break;
      case YELLOW:
	      return string("Yellow"); break;
      case BROWN: 
	      return string("Brown"); break;
      default: return string("Unknown"); }
   }
}

  
 // namespace animalfarm
