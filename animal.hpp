///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Kayla Valera <kvalera@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   @todo 20_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

#include <string>

namespace animalfarm {

enum Gender { MALE=0, FEMALE, UNKNOWN };

enum Color { BLACK, WHITE, RED, BLUE, GREEN, PINK, SILVER, YELLOW, BROWN };

class Animal {
public:
	enum Gender gender;
	string      species;

	virtual const string speak() = 0;
	
	void printInfo();
	
string bool2string (bool input);

	string colorName  (enum Color color);
	string genderName (enum Gender gender);
Animal(); //Declare Constructor
~Animal(); //Declare Destructor

};

} // namespace animalfarm
