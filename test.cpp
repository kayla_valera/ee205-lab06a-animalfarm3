###############################################################################
#University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 06a - Animal Farm 3
#
# @file    test.cpp
# @version 1.0
#
# @author Kayla Valera <kvalera@hawaii.edu>
# @brief  Lab 06a - Animal Farm 3 - EE 205 - Spr 2021
# @date   @todo 20_MAR_2021
###############################################################################
#include <iostream>
#include <time.h>
#include <random>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#define WEIGHT_MAX (200.00)
#define WEIGHT_MIN (20.00)
#define COLOR_MAX (6)
#define GENDER_MAX (2)
#define ANIMAL_TYPES (6)

using namespace std;
using namespace animalfarm;

static const Gender getRandomGender()
{
	   return (Gender) (rand() % GENDER_MAX);
};


static const Color getRandomColor(){
	 
	   return (Color) (rand() % COLOR_MAX);

	    };

static const bool getRandomBool(){

	   if ( rand() % 2 == 0)
		         return true;
	      else return false;

	       };
static const float getRandomWeight( const float min , const float max ){

	   return ((max - min) * ((float)rand() / RAND_MAX)) + min;
};

static const char getRandomConsonant(){
	  char consonant[] = {'b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z'};
	     return consonant[rand () % 20];
	        };

static const char getRandomVowel(){
	   char vowels[] = {'a','e','i','o','u','y'};
	      return vowels[rand() % 6];
};

static const char getRandomChar(){
	   int random = rand()% 2;
	    if(random==0){
	return getRandomConsonant();}
	         else{
	return getRandomVowel();
			          }
};
   

static const string getRandomName(){

	      char consonant[] = {'b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z'};

	        int length = rand () % 5 + 4; 
		char randomName[length];
		int random= rand()& 2;
	        int consonantcheck=0;
		randomName[0]=toupper( getRandomChar() );     

	for (int i=1;i<length;i++){

	for(int j=0;j<19;j++){
	
	if (randomName[i-1]==consonant[j]){
											        consonantcheck=1;
													         }
										             else{
												 consonentcheck=0;
	}
											        }
	
	if(consonentcheck==1){
										                   randomName[i]=getRandomVowel();
												               }
									                   else{
												   randomName[i]=getRandomChar();
														              }
											         }
return randomName; 
};

class AnimalFactory {
	   public:
static Animal* getRandomAnimal();
};


Animal* AnimalFactory::getRandomAnimal()
	   {
Animal* newAnimal = NULL;
int i = 0;
switch (i)  {
				               
	
case 0: newAnimal = new Cat (getRandomName() , getRandomColor(), getRandomGender());
		break;

case 1: newAnimal = new Dog ( getRandomName(), getRandomColor(), getRandomGender() );
		break;

case 2: newAnimal = new Nunu ( getRandomBool() , RED ,getRandomGender());
											                break;   

case 3: newAnimal = new Aku ( getRandomWeight(WEIGHT_MIN, WEIGHT_MAX) , SILVER, getRandomGender() );											        break;

											case 4: newAnimal = new Palila ( getRandomName() , YELLOW, getRandomGender() );
													break;

								
case 5: newAnimal = new Nene (getRandomName(), BROWN, getRandomGender() );
		break;
}

	return newAnimal;

	};

int main() {
	//random number generators

srand (time(NULL));

auto a = getRandomName();

cout << a << end1;

}
